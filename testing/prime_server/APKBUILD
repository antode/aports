# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=prime_server
pkgver=0.6.7
pkgrel=0
_commit_logging=39f2e39273c625d96790
_commit_testing=ada902fb51a1ad1e5a27
pkgdesc="Non-blocking (web)server API for distributed computing and SOA based on zeromq"
url="https://github.com/kevinkreiser/prime_server"
# s390x blocked by czmq
arch="all !s390x"
license="BSD-2-Clause"
makedepends="
	autoconf
	automake
	bash
	cmake
	curl-dev
	czmq-dev
	libtool
	zeromq-dev
	"
subpackages="$pkgname-dev"
source="https://github.com/kevinkreiser/prime_server/archive/$pkgver/prime_server-$pkgver.tar.gz
	logging-$_commit_logging.hpp::https://gist.githubusercontent.com/kevinkreiser/$_commit_logging/raw
	testing-$_commit_testing.hpp::https://gist.githubusercontent.com/kevinkreiser/$_commit_testing/raw
	0001-fix-cmake.patch
	"

prepare() {
	default_prepare

	mv "$srcdir/logging-$_commit_logging.hpp" prime_server/logging.hpp
	mv "$srcdir/testing-$_commit_testing.hpp" test/testing/testing.hpp
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	cd build

	case "$CARCH" in
		mips64) _skip_tests="interrupt"
	esac
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "$_skip_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="3fd66d5a39a220c2a43f0ee9237fca55b5bba5fddda88ee55d9d7f5ec9108acb06b8f36f45269b8b270b79a287714dc89a81e9524925762568326bea404ee712  prime_server-0.6.7.tar.gz
161504d5a18893a4e544c136de6295926958861c6937e16fcc3ffdda67d0905beb06d1c4eedc4a8c4cd0316982565906f0c70e3a9c13c8954c2e7355b70bbf1b  logging-39f2e39273c625d96790.hpp
0ebdb0d0ef5184357b4090bca1083bb351103a8ec05994c56114ee23590956a9aa3fb228c29b3d37885d1e9eb98e4c631c0e91e20f070a22c0509ef08085f2ba  testing-ada902fb51a1ad1e5a27.hpp
d73a5a448fd1bdc114e2edbdee179761a0d102b686dd55c677df1395a4aa330b37fd5b3df95946db57927db53d57c30bc58abd70b3593f820ccdcf6a141aeb62  0001-fix-cmake.patch"
