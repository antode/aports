# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Timo Teräs <timo.teras@iki.fi>
pkgname=youtube-dl
pkgver=2021.03.03
pkgrel=0
pkgdesc="Command-line program to download videos from YouTube"
url="https://youtube-dl.org/"
arch="noarch"
license="Unlicense"
depends="python3"
checkdepends="py3-flake8 py3-nose"
subpackages="$pkgname-doc
	$pkgname-zsh-completion:zshcomp
	$pkgname-bash-completion
	$pkgname-fish-completion"
source="https://youtube-dl.org/downloads/$pkgver/youtube-dl-$pkgver.tar.gz"
builddir="$srcdir/$pkgname"

prepare() {
	default_prepare
	sed -i \
		-e 's|etc/bash_completion.d|share/bash-completion/completions|' \
		-e 's|etc/fish/completions|share/fish/completions|' \
		"$builddir"/setup.py
}

build() {
	python3 setup.py build
}

check() {
	PYTHON=/usr/bin/python3 make offlinetest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

zshcomp() {
	pkgdesc="Zsh completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	install -Dm644 "$builddir"/$pkgname.zsh \
		"$subpkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="0a554ee6805ceaa1c68eab83be3bd6046bc56a26658c705e231f907c2970cdeb441e0ae57d5013385810b5bbee7e4de3ac7f6df36c3eef76a6fa9c5424b921a2  youtube-dl-2021.03.03.tar.gz"
